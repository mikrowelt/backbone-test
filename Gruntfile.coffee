module.exports = (grunt)->

  grunt.initConfig
    coffeelint:
      main: ['src/**/*.coffee', 'src/*.coffee']
      test: ['tests/*.coffee', 'tests/**/*.coffee', 'tests/**/**/*.coffee']
    coffee:
      main:
        options:
          bare: true
        expand: true
        flatten: false
        src: ['*.coffee', '**/*.coffee']
        dest: 'temp/src/'
        ext: '.js'
        cwd: './src'
      test:
        options:
          bare: true
        expand: true
        flatten: false
        src: ['*.coffee', '**/*.coffee']
        dest: 'temp/tests/'
        ext: '.js'
        cwd: './tests'
    browserify:
      main:
        src: ['./temp/src/main.js']
        dest: './dist/js/app.js'
        cwd: './'
        options:
          browserifyOptions:
            debug: true
      test:
        src: './temp/tests/main.js'
        dest: './dist/js/tests.js'
        cwd: './'
        options:
          browserifyOptions:
            debug: true
    testem:
      test:
        src: [
          'dist/js/tests.js'
        ]
        serve_files: ['dist/app.js']
        options:
          parallel: 2,
          launch_in_ci: ['Chrome']
          launch_in_dev: ['Chrome']

    copy: 
      main:
        src: 'src/index.html'
        dest: 'dist/index.html'
    sass:
      main:
        files:
          'dist/styles/main.css': 'src/styles/main.sass'


  grunt.loadNpmTasks('grunt-browserify')
  grunt.loadNpmTasks('grunt-coffeelint')
  grunt.loadNpmTasks('grunt-contrib-coffee')
  grunt.loadNpmTasks('grunt-contrib-testem')
  grunt.loadNpmTasks('grunt-contrib-copy')
  grunt.loadNpmTasks('grunt-contrib-sass')

  grunt.registerTask('build', ['coffeelint:main', 'coffee', 'browserify', 'copy', 'sass'])

  grunt.registerTask('test', ['coffeelint', 'coffee', 'browserify:test', 'sass', 'testem:test'])

  