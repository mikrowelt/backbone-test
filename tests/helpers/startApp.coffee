$ = require('jquery')

module.exports = ->

  $('body').append($('<div id="app-cont"></div>'))

  App = require('../../src/app')
  AppRouter = require('../../src/router')
  AppController = require('../../src/controller')
  Backbone = require('backbone')

  app = new App()

  app.router = new AppRouter(
    controller: new AppController(app: app)
  )

  #Init modules
  app.module 'index', require('../../src/modules/index')
  app.module 'search', require('../../src/modules/search/search')
  app.module 'search.content',
    require('../../src/modules/search/search-content')
  app.module 'search.filter',
    require('../../src/modules/search/search-filter')
  app.module 'main-menu', require('../../src/modules/main-menu')

  #app.on 'start', ->
  #  Backbone.history.start()

  return app
