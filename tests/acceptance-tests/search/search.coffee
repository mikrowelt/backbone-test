startApp = require('../../helpers/startApp')

app = false

$ = require('jquery')

Backbone = require('backbone')

QUnit.module 'search',

  beforeEach: (assert)->
    done = assert.async()
    app = startApp()
    app.on('start', ->
      done()
    )
    app.start()

  afterEach: ->
    app = false

QUnit.test 'all components exist', (assert)->

  Backbone.history.loadUrl('search')

  assert.expect(3)

  assert.equal($('.ts-search-item').length, 24,
    'All items rendered successfully')
  assert.equal($('.ts-search-filter').length, 1,
    'Filter rendered successfully')
  assert.equal($('.ts-filter-colors > option').length, 7,
    'Filter colors rendered successfully')
  
###
QUnit.test 'filters work', (assert)->

  Backbone.history.loadUrl('search')

  assert.expect(3)

  assert.equal($('.ts-search-item').length, 24,
    'All items rendered successfully')
  assert.equal($('.ts-search-filter').length, 1,
    'Filter rendered successfully')
  assert.equal($('.ts-filter-colors > option').length, 7,
    'Filter colors rendered successfully')
###