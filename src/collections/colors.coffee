Backbone = require('backbone')

ColorsCollection = Backbone.Collection.extend

  getColorById: (id)->
    return @get(id)

  getColorLabelById: (id)->
    return @getColorById(id)?.get('label')
    

module.exports = ColorsCollection
