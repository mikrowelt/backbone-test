Backbone = require('backbone')

MainMenuCollection = Backbone.Collection.extend

  setActiveItem: (alias) ->
    @forEach (item)->
      item.set('isActive', item.get('alias') == alias)

module.exports = MainMenuCollection
