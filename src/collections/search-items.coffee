Backbone = require('backbone')

SearchItemsCollection = Backbone.Collection.extend

  getFiltered: ->
    return @toJSON()

module.exports = SearchItemsCollection
