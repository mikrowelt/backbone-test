Mn = require('backbone.marionette')
appTemplate = require('../templates/app')
MainMenuView = require('../views/main-menu/main-menu')

MainLayout = Mn.LayoutView.extend

  template: appTemplate

  el: '#app-cont'

  regions:
    header: '#header'
    main: '#main'
    footer: '#footer'
  

module.exports = MainLayout