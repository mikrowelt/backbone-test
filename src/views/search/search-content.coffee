Mn = require('backbone.marionette')
SearchContentItemView = require('../../views/search/search-content-item')
SearchContentEmptyView = require('../../views/search/search-content-empty')

SearchContentView = Mn.CollectionView.extend

  childView: SearchContentItemView

  emptyView: SearchContentEmptyView

  filter: (model, index, collection)->
    filter = @filterModel.toJSON()
    price = parseInt(model.get('price'))
    return (!filter.priceMin || parseInt(filter.priceMin) < price) &&
      (!filter.priceMax || parseInt(filter.priceMax) > price) &&
      (!filter.color || parseInt(filter.color) == parseInt(model.get('color')))

  initialize: (options)->
    @filterModel = options.filterModel
    @listenTo(@filterModel, 'change', =>
      @render()
    )
    @render()



module.exports = SearchContentView
