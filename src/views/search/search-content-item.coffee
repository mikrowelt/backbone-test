Mn = require('backbone.marionette')
itemTemplate = require('../../templates/search/search-content-item')

MainMenuItemView = Mn.ItemView.extend

  template: itemTemplate
  
  serializeModel: ->
    colors = @getOption('colors')
    result = @model.toJSON()
    result.color = colors.getColorLabelById(result.color)
    return result

module.exports = MainMenuItemView
