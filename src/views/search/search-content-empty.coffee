Mn = require('backbone.marionette')
emptyTemplate = require('../../templates/search/search-content-empty')

SearchContentEmptyView = Mn.ItemView.extend

  template: emptyTemplate
  

module.exports = SearchContentEmptyView
