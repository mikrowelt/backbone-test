Mn = require('backbone.marionette')
searchFilterTemplate = require('../../templates/search/search-filter')
$ = require('jquery')

SearchFilterView = Mn.ItemView.extend

  template: searchFilterTemplate

  events:
    'input .js-price-min': 'filterChanged'
    'input .js-price-max': 'filterChanged'
    'change .js-color-selector': 'filterChanged'

  filterChanged: (e)->
    target = $(e.target)
    type = target.attr('data-type')
    inputValue = target.val()
    modelValue = @model.get(type)
    if not inputValue
      return true
    if inputValue != modelValue
      @model.set type, inputValue,
        validate: true
  
  initialize: (options)->
    @model.on('invalid', (model, error)=>
      @$el.find('[data-type=' + error + ']').addClass('error')
    )

  serializeModel: ->
    colors = @getOption('colors')
    filter = @filter
    result =
      filter: filter
      colors: colors
    return result


module.exports = SearchFilterView
