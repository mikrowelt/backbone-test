Mn = require('backbone.marionette')
searchTemplate = require('../../templates/search/search')

MainMenuLayout = Mn.LayoutView.extend

  template: searchTemplate

  regions:
    content: '.search-content'
    filter: '.search-filter'


module.exports = MainMenuLayout
