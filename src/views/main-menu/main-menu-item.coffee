Mn = require('backbone.marionette')
itemTemplate = require('../../templates/main-menu/main-menu-item')

MainMenuItemView = Mn.ItemView.extend

  template: itemTemplate

  tagName: 'li'

  initialize: ->
    @listenTo(@model, 'change', @render.bind(@))

  render: ->
    active = @model.get('isActive')
    if active
      @$el.addClass('active')
    else
      @$el.removeClass('active')
    Mn.ItemView.prototype.render.apply(@, arguments)
  

module.exports = MainMenuItemView
