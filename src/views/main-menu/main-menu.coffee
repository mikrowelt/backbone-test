Mn = require('backbone.marionette')
MainMenuItemView = require('../../views/main-menu/main-menu-item')

MainMenuLayout = Mn.CollectionView.extend

  childView: MainMenuItemView

  className: 'nav nav-pills'

module.exports = MainMenuLayout
