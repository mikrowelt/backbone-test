Mn = require('backbone.marionette')
func = Mn.Module._addModuleDefinition
Mn.Module._addModuleDefinition = (parentModule, module)->
  module.parent = parentModule
  func.apply(@, arguments)
