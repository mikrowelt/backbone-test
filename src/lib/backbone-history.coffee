module.exports = (appRoot)->
  $ = require('jquery')
  Backbone = require('backbone')

  Backbone.history.start
    pushState: true
    root: appRoot

  $(document).on 'click', 'a[href]:not([data-bypass])', (evt)->
    href =
      prop: $(this).prop('href')
      attr: $(this).attr('href')

    root = location.protocol + '//' + location.host + appRoot

    if href.prop.slice(0, root.length) == root
      evt.preventDefault()

      Backbone.history.navigate href.attr, true
