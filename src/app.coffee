Mn = require('backbone.marionette')
AppLayout = require('./views/app')

App = Mn.Application.extend

  shownModule: null

  rootView: null

  showModule: (moduleToShow) ->
    if @shownModule
      @shownModule.stop()
    @rootView.getRegion('main').show(moduleToShow.rootView)
    @shownModule = moduleToShow

  onBeforeStart: ->
    @rootView = new AppLayout()
    @rootView.render()

module.exports = App
