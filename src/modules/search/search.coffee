Mn = require('backbone.marionette')
SearchLayout = require('../../views/search/search')
SearchItemsCollection = require('../../collections/search-items')
searchItemsFixture = require('../../fixtures/search-items')
SearchFilterModel = require('../../models/search-filter')
ColorsCollection = require('../../collections/colors')
colorsFixtures = require('../../fixtures/colors')

SearchModule = Mn.Module.extend

  startWithParent: false

  initialize: (moduleName, app)->
    @app = app
    @searchFilter = new SearchFilterModel()
    @searchItems = new SearchItemsCollection(searchItemsFixture)
    @colors = new ColorsCollection(colorsFixtures)

  onBeforeStart: (options)->
    @searchFilter.clear()
    @rootView = new SearchLayout()
    
    
  onStart: ->
    @rootView.render()
    @initComponents()

  initComponents: ->
    #content module
    contentRegion = @rootView.getRegion 'content'
    content = @app.module 'search.content'
    content.on 'start', ->
      contentRegion.show(content.rootView)
    content.start()
    #filter module
    filterRegion = @rootView.getRegion 'filter'
    filter = @app.module 'search.filter'
    filter.on 'start', ->
      filterRegion.show(filter.rootView)
    filter.start()
    

    

module.exports = SearchModule
