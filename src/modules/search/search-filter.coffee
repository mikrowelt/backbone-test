Mn = require('backbone.marionette')
SearchFilterView = require('../../views/search/search-filter')

SearchModule = Mn.Module.extend

  startWithParent: false

  initialize: (moduleName, app)->
    @app = app


  onStart: (options)->
    filter = @parent.searchFilter
    colors = @parent.colors
    @rootView = new SearchFilterView
      model: filter
      colors: colors
    @rootView.render()

module.exports = SearchModule
