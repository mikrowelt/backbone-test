Mn = require('backbone.marionette')
SearchContentView = require('../../views/search/search-content')
SearchItemsCollection = require('../../collections/search-items')
searchItemsFixture = require('../../fixtures/search-items')

SearchModule = Mn.Module.extend

  startWithParent: false

  initialize: (moduleName, app)->
    @app = app

  onStart: (options)->
    collection = @parent.searchItems
    filter = @parent.searchFilter
    colors = @parent.colors
    @rootView = new SearchContentView
      collection: collection
      filterModel: filter
      childViewOptions:
        colors: colors
    @rootView.render()

module.exports = SearchModule
