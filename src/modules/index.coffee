Mn = require('backbone.marionette')
IndexRootView = require('../views/index')

IndexModule = Mn.Module.extend

  startWithParent: false

  initialize: (moduleName, app)->
    @app = app
    
    
  onBeforeStart: (options)->
    @rootView = new IndexRootView()
  

module.exports = IndexModule
