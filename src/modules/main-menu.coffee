Mn = require('backbone.marionette')
MainMenuView = require('../views/main-menu/main-menu')
MainMenuCollection = require('../collections/main-menu')
mainMenuFixture = require('../fixtures/main-menu')

MainMenuModule = Mn.Module.extend

  initialize: (moduleName, app)->
    @app = app
    @MenuItems = new MainMenuCollection(mainMenuFixture)
    
  onStart: ->
    @rootView = new MainMenuView(collection: @MenuItems)
    region = @app.rootView.getRegion 'header'
    region.show(@rootView)

module.exports = MainMenuModule
