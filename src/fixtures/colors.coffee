module.exports = [
  {
    id: 1
    label: 'green'
  },
  {
    id: 2
    label: 'red'
  },
  {
    id: 3
    label: 'yellow'
  },
  {
    id: 4
    label: 'blue'
  },
  {
    id: 5
    label: 'white'
  },
  {
    id: 6
    label: 'black'
  }
]
