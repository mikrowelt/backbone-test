module.exports = [
  {
    label: 'Home',
    url: '/',
    alias: 'index',
    isActive: false
  },
  {
    label: 'Search',
    url: '/search',
    alias: 'search',
    isActive: false
  }
]
