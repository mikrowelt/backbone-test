module.exports = [
  {
    name: 'product 1'
    color: 1
    price: 300
  },
  {
    name: 'product 2'
    color: 3
    price: 100
  },
  {
    name: 'product 3'
    color: 4
    price: 400
  },
  {
    name: 'product 4'
    color: 2
    price: 500
  },
  {
    name: 'product 5'
    color: 6
    price: 600
  },
  {
    name: 'product 6'
    color: 5
    price: 800
  },
  {
    name: 'product 11'
    color: 2
    price: 300
  },
  {
    name: 'product 12'
    color: 4
    price: 100
  },
  {
    name: 'product 13'
    color: 1
    price: 400
  },
  {
    name: 'product 14'
    color: 6
    price: 500
  },
  {
    name: 'product 15'
    color: 2
    price: 600
  },
  {
    name: 'product 16'
    color: 6
    price: 800
  },
  {
    name: 'product 21'
    color: 4
    price: 300
  },
  {
    name: 'product 22'
    color: 1
    price: 100
  },
  {
    name: 'product 23'
    color: 2
    price: 400
  },
  {
    name: 'product 24'
    color: 5
    price: 500
  },
  {
    name: 'product 25'
    color: 6
    price: 600
  },
  {
    name: 'product 26'
    color: 3
    price: 800
  },
  {
    name: 'product 31'
    color: 5
    price: 300
  },
  {
    name: 'product 32'
    color: 3
    price: 100
  },
  {
    name: 'product 33'
    color: 4
    price: 400
  },
  {
    name: 'product 34'
    color: 1
    price: 500
  },
  {
    name: 'product 35'
    color: 5
    price: 600
  },
  {
    name: 'product 36'
    color: 1
    price: 800
  },
]
