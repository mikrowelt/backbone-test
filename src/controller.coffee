Mn = require('backbone.marionette')

AppController = Mn.Controller.extend

  initialize: (options) ->
    @app = options.app

  index: ->
    indexModule = @app.module 'index'
    indexModule.on 'before:start', =>
      @app.showModule(indexModule)
      mainMenuModule = @app.module 'main-menu'
      mainMenuModule.MenuItems.setActiveItem('index')
    indexModule.start()

  search: ->
    searchModule = @app.module 'search'
    searchModule.on 'before:start', =>
      @app.showModule(searchModule)
      mainMenuModule = @app.module 'main-menu'
      mainMenuModule.MenuItems.setActiveItem('search')
    searchModule.start()
    

module.exports = AppController
