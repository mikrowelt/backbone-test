Mn = require('backbone.marionette')

Router = Mn.AppRouter.extend
  
  appRoutes:
    "": "index"
    "search": "search"

  initialize: (options)->
    @app = options.app
    

module.exports = Router
