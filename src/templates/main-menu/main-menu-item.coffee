_ = require('underscore')

module.exports = _.template(

  '<a href="<%= url %>">' +
  '<%= label %>' +
  '</a>'
)
