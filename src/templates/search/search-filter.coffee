_ = require('underscore')

module.exports = _.template(
  '<form class="ts-search-filter">' +
  '<div class="form-group">' +
  '<label>Price: </label>' +
  '<input type="text" class="js-price-min form-control" data-type="priceMin"' +
  ' placeholder="min">' +
  '<input type="text" data-type="priceMax" class="form-control js-price-max"' +
  ' placeholder="max">' +
  '</div>' +
  '<div class="form-group">' +
  '<select data-type="color"' +
  'class="js-color-selector form-control ts-filter-colors">' +
  '<option value="">Any color</option>' +
  '<% colors.forEach(function(color) { %>' +
  '<option value="<%= color.get("id") %>">' +
  '<%= color.get("label") %>' +
  '</option><% }) %>' +
  '</select></div>' +
  '</form>'
)
