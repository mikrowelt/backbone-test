_ = require('underscore')

module.exports = _.template(
  '<div class="row">' +
  '<div class="col-md-3 search-filter">' +
  '</div>' +
  '<div class="col-md-9 search-content">' +
  '</div>' +
  '</div>'
)
