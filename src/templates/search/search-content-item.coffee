_ = require('underscore')

module.exports = _.template(
  '<div class="thumbnail search-item-cont ts-search-item">' +
  '<div class="caption">' +
  '<h3><%= name %></h3>' +
  '<p>color: <%= color %></p>' +
  '<p>price: <%= price %></p>' +
  '</div>' +
  '</div>'
  
)
