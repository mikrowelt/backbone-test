_ = require('underscore')

module.exports = _.template(
  '<div class="container">' +
  '<div class="row" >' +
  '<div id="header" class="col-md-12">header</div>' +
  '</div>' +
  '<div class="row">' +
  '<div id="main" class="col-md-12">main</div>' +
  '</div>' +
  '<div class="row">' +
  '<div id="footer" class="col-md-12">footer</div>' +
  '</div>' +
  '</div>'
)
