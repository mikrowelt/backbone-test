Backbone = require('backbone')

SearchFilterModel = Backbone.Model.extend

  defaults:
    color: false
    priceMin: false
    priceMax: false

  validate: (attrs) ->
    if attrs.priceMin and parseInt(attrs.priceMin) < 0
      return 'priceMin'
    if attrs.priceMax and parseInt(attrs.priceMax) > 10000
      return 'priceMax'


module.exports = SearchFilterModel
