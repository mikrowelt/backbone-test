require('./lib/marionette-depends')
require('./lib/marionette-module')

App = require('./app')
AppRouter = require('./router')
AppController = require('./controller')
Backbone = require('backbone')

app = new App()

app.router = new AppRouter(
  controller: new AppController(app: app)
)

#Init modules
app.module 'index', require('./modules/index')
app.module 'search', require('./modules/search/search')
app.module 'search.content', require('./modules/search/search-content')
app.module 'search.filter', require('./modules/search/search-filter')
app.module 'main-menu', require('./modules/main-menu')

app.on 'start', ->
  require('./lib/backbone-history')('/')

app.start()
